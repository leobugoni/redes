```cpp
#include < Ultrasonic.h >

Ultrasonic ultrasonic(2, 3); // (Trig PIN,Echo PIN)

const int motorPin1 = 9; // Pin 14 of L293
const int motorPin2 = 10; // Pin 10 of L293

const int motorPin3 = 6; // Pin 7 of L293
const int motorPin4 = 5;

int modo = 12;
int modoControlo;

void setup() {
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);
  pinMode(motorPin3, OUTPUT);
  pinMode(motorPin4, OUTPUT);

  Serial.begin(9600);

}

void loop() {
  modoControlo = digitalRead(modo); //interruptor para selecionar o modo

  if (modoControlo == HIGH) {
    controloAutomato();
  } else {
    controloBluetooth();
  }
}

void controloAutomato() { // Funcao automatico quando selecionada, o robot desvia-se sozinho dos obstaculos

  if (ultrasonic.Ranging(CM) >= 8) {
    analogWrite(motorPin1, 0);
    analogWrite(motorPin2, 150);
    analogWrite(motorPin3, 160);
    analogWrite(motorPin4, 0);
  } else {
    //Recuar durante 900 ms
    analogWrite(motorPin1, 155);
    analogWrite(motorPin2, 0);
    analogWrite(motorPin3, 0);
    analogWrite(motorPin4, 160);
    delay(900);

    //Virar para a direita durante 700ms
    analogWrite(motorPin1, 150);
    analogWrite(motorPin2, 0);
    analogWrite(motorPin3, 160);
    analogWrite(motorPin4, 0);
    Serial.println("Esquerda");
    delay(500);

  }
}

void controloBluetooth() { // Funcao manual ativada, ele aguarda comandos enviados pelo smartphone para se movimentar
  if (Serial.available() > 0) {
    char data;
    data = Serial.read();
    Serial.write(Serial.read());

    analogWrite(motorPin1, 0);
    analogWrite(motorPin2, 0);
    analogWrite(motorPin3, 0);
    analogWrite(motorPin4, 0);

    switch (data) {
    case 'F': //FORWARD
      analogWrite(motorPin1, 0);
      analogWrite(motorPin2, 150);
      analogWrite(motorPin3, 160);
      analogWrite(motorPin4, 0);
      break;
    case 'B': //REVERSE
      analogWrite(motorPin1, 150);
      analogWrite(motorPin2, 0);
      analogWrite(motorPin3, 0);
      analogWrite(motorPin4, 160);
      break;
    case 'L': //FORWARD LEFT
      analogWrite(motorPin1, 0);
      analogWrite(motorPin2, 150);
      analogWrite(motorPin3, 0);
      analogWrite(motorPin4, 0);
      break;
    case 'R': //FORWARD RIGHT
      analogWrite(motorPin1, 0);
      analogWrite(motorPin2, 0);
      analogWrite(motorPin3, 160);
      analogWrite(motorPin4, 0);
      break;

    default: //se o bluetooth não receber informação, ele permanesse imovel por razoes de segurança.
      analogWrite(motorPin1, 0);
      analogWrite(motorPin2, 0);
      analogWrite(motorPin3, 0);
      analogWrite(motorPin4, 0);
    }
  }
}
```
